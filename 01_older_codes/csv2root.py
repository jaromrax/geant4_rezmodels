#!/usr/bin/env python3
"""

this is needed to converst csv to root.


there is pyg4ometry code that creates gdml
and compile sh that prepares everything.

I dont know if project could help here:
- compilation OK
- merging csv OK
- geometrry NO
- breakout to root NO

"""

from fire import Fire
import ROOT
import pandas as pd
import numpy as np
import time
import datetime as dt

def main(infile = "merged.csv"):

    print(" ... reading csv", infile)
    now = dt.datetime.now()
    df = pd.read_csv(infile, header=None,comment="#")
    print(f"i... ... time spent {dt.datetime.now()-now} ")

    print(df.head() )
    print( f"... columns: {len(df.columns)} ")
    print( f"...  length: {len(df)}        to_numpy:")

    now = dt.datetime.now()
    data = df[1].to_numpy()
    print(f"i... ... time spent {dt.datetime.now()-now} ")

    emax = np.max(data)
    print(" ... emax = ", emax)
    nmax=[1,10,100,1000]
    # set maximum
    for imax in nmax:
        # print(f" ... {emax} < {imax} ?" )
        if emax<imax:
            emax = imax
            break

####################################################################
#
# this is the correct? way to get TH1 binning. It is consistent with ROOT
#  where TH1F *h = new TH1F("A","A", 10000,0,10);h->Fill(3.022);
#     for (int i=3020;i<3029; i++){ float v=h->GetBinContent(i); printf("%d %f\n", i,v);}
# GIVES channel 3022;   for 10010 bins it is clear.
####################################################################
    NBINS = 1000
    # max value is 3.022, check the bin;;  +1 no, -1 no
    now = dt.datetime.now()
    # I need to match with TH1;(0, emax, num=NBINS+1) THIS  really gives NBINS   bins, edges 0 and emax
    bins = np.linspace(0, emax, num=NBINS+1)
    #
    nphist = np.histogram(data, bins, range=( 0.0, emax ) )
    print(f"i... ... time spent {dt.datetime.now()-now} ")
    print(nphist[1])

    print(" >>>> edges , bins",  len(nphist[1]) , len(nphist[0])  ) # -====


    hist = ROOT.TH1F('hist_name', 'Histogram Title', NBINS, 0, emax) # BIN 0 I underflow
    ihi = 0
    for i in range(NBINS):  # from 0 to last bin
        hist.SetBinContent( i+1, nphist[0][i] )  # BIN 0  IS UNDERF


#    for value in data:  # uses numpy
#        hist.Fill(value)

    hist.Print()
    print( hist.GetXaxis().GetXmin() , hist.GetXaxis().GetXmax() , " centers of 1 and NBINS"  ,hist.GetBinCenter(1), hist.GetBinCenter(NBINS)   )
    hist.Draw()
    #time.sleep(1)
    print("K... press enter ")
    input()


if __name__=="__main__":
    Fire(main)
