
#include "G04SensitiveDetector.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

// get track
#include "G4RunManager.hh"
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4Gamma.hh"
#include "G4Neutron.hh"
#include "G4Proton.hh"
// not needed #include "G4Ions.hh"


#include "G4AnalysisManager.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// my:  nanosecods
#include "G4SystemOfUnits.hh"
// i need to know vis or run
#include "global_info.hh"
#include <map>

// =======hadr03 ====================
#include "G4HadronicProcess.hh"
#include "G4VProcess.hh"
#include "G4StepPoint.hh"


G04SensitiveDetector::G04SensitiveDetector(const G4String& name)
  : G4VSensitiveDetector(name)
{
  //  G4cout << "*****************Sensitive Detector CONSTRUCTOR..................................... "<<G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G04SensitiveDetector::~G04SensitiveDetector()
{
  //  G4cout << "Sensitive Detector DESTRUCTOR..................................... "<<G4endl;

  //auto analysisManager = G4AnalysisManager::Instance();
////analysisManager->Write();
////analysisManager->CloseFile();
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void G04SensitiveDetector::Initialize(G4HCofThisEvent*)
{

  // WATCH_EVT = 0;  //// WATCH THIS EVENT   in  global_info

  G4int evt = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  //  G4cout << "Sensitive Detector Initialize.....................................evt: "<<evt<<G4endl;
  Etot = 0.0;
  for (int i=0;i<maxmap;i++){  demap[i]=0.0;}  // tuple_map_local

  // is not a  reaction ... for whole event
  is_reaction = false;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool G04SensitiveDetector::ProcessHits(G4Step* step, G4TouchableHistory* h)
{
  //G4cout << "Sensitive Detector    HIT..................................... "<<G4endl;


  // =================== MATERIAL WORKS =============
  //G4StepPoint* preStepPoint = step->GetPreStepPoint();
  //G4LogicalVolume* logicalVolume = preStepPoint->GetTouchableHandle()->GetVolume()->GetLogicalVolume();
  //G4Material* material = logicalVolume->GetMaterial();
  // G4cout<<material<<G4endl;

  //  G4double de = s->GetDeltaEnergy();
  G4double edep = step->GetTotalEnergyDeposit();
  G4double dt = step->GetDeltaTime();
  dt=dt+step->GetPreStepPoint()->GetGlobalTime();   // for activation HUGE



  // hard03 =======================  RadioActivation process .................
  const G4StepPoint* endPoint = step->GetPostStepPoint();
  G4VProcess* process   = const_cast<G4VProcess*>(endPoint->GetProcessDefinedStep());
  G4HadronicProcess* hproc = dynamic_cast<G4HadronicProcess*>(process);
  const G4Isotope* target = NULL;
  if (hproc) target = hproc->GetTargetIsotope();
  G4String targetName = "XXXX";
  if (target) {
    targetName = target->GetName();
    is_reaction = true;
    //    G4cout<<"***********************************************************"<<G4endl<<G4endl;
  }
  // G4double dt = step->GetPostStepPoint()->GetGlobalTime(); // previous is more correct



  // check that an real interaction occured (eg. not a transportation)
  G4StepStatus stepStatus = endPoint->GetStepStatus();
  G4bool transmit = (stepStatus==fGeomBoundary || stepStatus==fWorldBoundary);
  if (transmit) return true;

  //  G4double stepLength = aStep->GetStepLength();
  //  run->SumTrack(stepLength);

  // =============================== END OF HARD03

  G4String ss=" - ";
  //  G4String part="--";
  char part[10];  // Here I have the text to print

  const int maxelem = 34;
  const char* elem[maxelem]
  = { "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "33", "34" };


  auto touchable = step->GetPreStepPoint()->GetTouchable();
  auto motherPhysical = touchable->GetVolume(1); // 2022 5 11  mother
  auto physical = touchable->GetVolume();
  auto copyNo = physical->GetCopyNo();
  auto motherCopyNo = touchable->GetVolume(1)->GetCopyNo();

  if (h != 0){
    ss = h->GetVolume()->GetName();  // never gives somt
  }

  G4bool las = step->IsLastStepInVolume(); // escaped the volume
  G4bool fir = step->IsFirstStepInVolume();

  // # event number
  G4int evt = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();



  G4Track *track = step->GetTrack();
  //  track->SetTrackStatus(fStopAndKill); // I REMOVE ALL SECONDARY TRACKS ????
  G4bool abs = track->GetTrackStatus();
  sprintf(part,"%s","--");

  G4ParticleDefinition* particle = track->GetDefinition();
  G4double Et = track->GetTotalEnergy();  // plus mass
  G4double Ek = track->GetKineticEnergy();  // seems this is Ek after LOSSES

  G4int tupin = tuple_map[physical->GetName()];
  //  G4cout<<" ///"<<physical->GetName()<<"//// tupin  demap [ "<<tupin<<" ]  ... gets "<<edep<<G4endl;
  demap[tupin]+=edep;
  //  if (evt>=4148487){
  //    G4cout<<evt<<"_______________________________"<< particle->GetAtomicMass() << G4endl;
  // }

  //  if ( (particle->GetAtomicMass()>0)   and (particle->GetAtomicNumber()+1<maxelem) ) {
  //    sprintf(part, "%d-%s", int(particle->GetAtomicMass() ), elem[int(particle->GetAtomicNumber() ) -1]  );
  //    //part="IO";
  //  }

  // if (particle == G4Electron::Definition()) { sprintf(part,"%s","e-");}
  // else if (particle == G4Positron::Definition()) { sprintf(part,"%s","e+");}
  // else if (particle == G4Gamma::Definition()) {  sprintf(part,"%s","ga");}
  // else if (particle == G4Proton::Definition()) {  sprintf(part,"%s","p0");}
  // else if (particle == G4Neutron::Definition()) {  sprintf(part,"%s","n0");}
  // else if ( (particle->GetAtomicMass()>0)   and (particle->GetAtomicNumber()+1<maxelem) ) {
  //   sprintf(part, "%d-%s", int(particle->GetAtomicMass() ), elem[int(particle->GetAtomicNumber() ) -1]  );
  //   //part="IO";
  // }else{
  //   sprintf(part,"%s", particle->GetParticleName() );
  // }

  //-------------------------------------------------------- PARTICLE DESCRIPBED ------------------------------
  //    if ((detailed_display) and (evt==200)){
  //      G4cout<<"... switching off detailed display..."<<G4endl;
  //      detailed_display = 0;
  //    }

  auto secondary = step->GetSecondaryInCurrentStep();
 size_t size_secondary = (*secondary).size();


 //------------------------display decision logic here:---------------
 //------------------------display decision logic here:---------------
 //------------------------display decision logic here:---------------
 G4bool display = false;
 //---------------------------  watching in vis / 0 / evt
 if ((detailed_display) || (evt==0) || (evt==WATCH_EVT) ){ display = true;}
 // --- OVERRIDE ------
 if (WATCH_REACTIONS){ display = is_reaction; }  // MOD in MAIN


 if (display){

    if ((size_secondary) and (is_reaction)){
      // CAN BE
// 2 12 process:phot
// 2 13 process:compt
// 2 2 process:eIoni
// 2 3 process:eBrem
// 4 111 process:hadElastic
// 4 121 process:alphaInelastic
// 4 121 process:dInelastic
// 4 121 process:ionInelastic
// 4 121 process:neutronInelastic
// 4 121 process:protonInelastic
// 4 121 process:tInelastic
// 6 210 process:Radioactivation

      G4cout<<"D... ========================== process:"<<process->GetProcessName()<<" "<<process->GetProcessType()<<" "<<process->GetProcessSubType();
      G4cout<< " sec# :"<<size_secondary<<G4endl<<G4endl;
    }

    //if (is_reaction){
    G4cout<<std::setw(8) << evt<<" __ "<<std::setw(2)<<track->GetParentID()<<":"<<std::setw(3)<<track->GetTrackID()<<" ";
    if (fir==1){ G4cout<<"F";}else{ G4cout<<" ";}
    if (las==1){ G4cout<<"L";}else{ G4cout<<" ";}
    if (abs==1){ G4cout<<"S";}else{ G4cout<<" ";}
    G4cout<<"  "<<std::setw(9) << particle->GetParticleName() << " + "<<targetName;
    //    <<std::setw(6)<<round(10000*Et)/10000
    G4cout<<": Eki="<<" "<<std::setw(8)<<round(10000*(Ek+edep))/10000<<"  DE=" << std::setw(6)<<round(10000*edep)/10000;
    //<< " " << "  f-l "<<fir <<"-"<< las<<"-"<<abs<<" ";
    G4cout<<"// "<<motherPhysical->GetName()<<":"<<motherCopyNo<<" "<<physical->GetName()<<":"<<copyNo;
    if (dt/s>1){
      G4cout<<"        t="<<round(10000*dt/s)/10000<<" s"<<G4endl;
    }else{
      G4cout<<"        t="<<round(10000*dt/ns)/10000<<" ns"<<G4endl;
    }
    //if (las){ G4cout<<G4endl;}
  }

  if (edep==0.) return true;
  //  Etot=1.0*Etot + 1.0*edep;
//  G4cout<< " ..."<< vol->GetName() <<G4endl;

//  if (evt>=76355){
//    G4cout<<evt<<"______________________________="<<G4endl;
//  }

  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void G04SensitiveDetector::EndOfEvent(G4HCofThisEvent* hc)
{
  G4int prec = G4cout.precision(3);
  G4int evt = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();

  //  if (evt>=76355){
  //    G4cout<<evt<<"________= = = _________________"<<G4endl;
  //  }
  G4int kilo = 1000;
  Etot = demap[1];
  if ( (evt<15) or ( evt == WATCH_EVT  ) ){
    G4cout << "End Of Event ........................ "<<std::setw(8)<< evt<<"  Etot1="<<std::setw(7)<<Etot<<G4endl;

    //G4cout << "End Of Event ............................ "<<std::setw(6)<< evt<<"  Etot="<<Etot<<G4endl;
  }
  else if ( evt%(100*kilo) == 0 ){
    G4cout << "End Of Event ........................ "<<std::setw(8)<< round(evt/1000)<<"k  Etot1="<<std::setw(7)<<Etot<<G4endl;
    //G4cout << "End Of Event ............................ "<<std::setw(6)<< evt<<"  Etot="<<Etot<<G4endl;
  }

  // ============ JUST GET INSTANVE
  if (not detailed_display){

    auto analysisManager = G4AnalysisManager::Instance();
    analysisManager->FillNtupleIColumn(0, evt);
    for (int i=1;i<=tuple_map.size();i++){ // 1 2  if size==2
      //G4cout<<" ... filling  tuple column..."<<i<<" "<<demap[i]<<"   /tuplesize="<<tuple_map.size()<<G4endl;
      //analysisManager->FillNtupleDColumn(1, Etot);
      analysisManager->FillNtupleDColumn(i, demap[i] );
    }
    // //analysisManager -> FillNtupleIColumn(1, parentID);
    // //analysisManager -> FillNtupleIColumn(2, volumeID);
    // //analysisManager -> FillNtupleSColumn(4, particleName);
    analysisManager->AddNtupleRow();
    // // not here analysisManager->Write();
  }
  //  G4cout << "End Of Event ............................ x"<<G4endl;

}
