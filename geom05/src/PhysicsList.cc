
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "PhysicsList.hh"
#include "G4UnitsTable.hh"
#include "G4ParticleTypes.hh"
#include "G4IonConstructor.hh"
#include "G4PhysicsListHelper.hh"
#include "G4Radioactivation.hh"
#include "G4SystemOfUnits.hh"
#include "G4NuclideTable.hh"
#include "G4LossTableManager.hh"
#include "G4UAtomicDeexcitation.hh"
#include "G4NuclideTable.hh"
#include "G4NuclearLevelData.hh"
#include "G4DeexPrecoParameters.hh"
#include "G4PhysListUtil.hh"
#include "G4EmBuilder.hh"
#include "globals.hh"

// rdecay02----------------------------
#include "G4EmStandardPhysics.hh"
#include "G4EmExtraPhysics.hh"
#include "G4EmParameters.hh"
#include "G4DecayPhysics.hh"
#include "G4NuclideTable.hh"
//#include "BiasedRDPhysics.hh"
#include "G4HadronElasticPhysics.hh"
#include "G4HadronPhysicsFTFP_BERT.hh"
#include "G4HadronInelasticQBBC.hh"
#include "G4HadronPhysicsINCLXX.hh"
#include "G4IonElasticPhysics.hh"
#include "G4IonPhysics.hh"
#include "G4IonINCLXXPhysics.hh"

// particles

#include "G4BosonConstructor.hh"
#include "G4LeptonConstructor.hh"
#include "G4MesonConstructor.hh"
#include "G4BosonConstructor.hh"
#include "G4BaryonConstructor.hh"
#include "G4IonConstructor.hh"
#include "G4ShortLivedConstructor.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "RadioactiveDecayPhysics.hh"

#include "HadronElasticPhysicsHP.hh"
#include "G4HadronPhysicsQGSP_BIC_HP.hh"
#include "G4IonPhysicsXS.hh"
#include "G4StoppingPhysics.hh"
#include "ElectromagneticPhysics.hh"

#include "G4EmStandardPhysics_option3.hh"
#include "G4HadronElasticPhysicsHP.hh"
#include "G4HadronPhysicsQGSP_BIC.hh"

#include "BiasedRDPhysics.hh"


#include "G4BosonConstructor.hh"
#include "G4LeptonConstructor.hh"
#include "G4MesonConstructor.hh"
#include "G4BosonConstructor.hh"
#include "G4BaryonConstructor.hh"
#include "G4IonConstructor.hh"

// =======================hadr03

#include "G4HadronElasticPhysicsXS.hh"
#include "GammaNuclearPhysicsLEND.hh"
#include "GammaNuclearPhysics.hh"
#include "G4RadioactiveDecayPhysics.hh"

PhysicsList::PhysicsList()  //:G4VModularPhysicsList()
{
 ///geant4/source/physics_lists/constructors
  //decay  electromagnetic  factory  gamma_lepto_nuclear  hadron_elastic  hadron_inelastic  History  ions  limiters  stopping
// /3rd_parties/geant4/source/physics_lists/lists/include
//  Hadronic  P precompound deexcitation(seems HE)    HP higq neutron <20MeV
  // user livermore(10eV-) or penelope(includes positrons)100eV-

  G4int verb = 1;
  SetVerboseLevel(verb);
  //
  const G4double meanLife = 1*picosecond;
  G4NuclideTable::GetInstance()->SetMeanLifeThreshold(meanLife);
  G4NuclideTable::GetInstance()->SetLevelTolerance(1.0*eV);

  //RD02===================================================

    // Mandatory for G4NuclideTable
  // Half-life threshold must be set small or many short-lived isomers
  // will not be assigned life times (default to 0)
  G4NuclideTable::GetInstance()->SetThresholdOfHalfLife(0.1*picosecond);
  G4NuclideTable::GetInstance()->SetLevelTolerance(1.0*eV);

  // EM physics
  RegisterPhysics(new G4EmStandardPhysics());
  G4EmParameters* param = G4EmParameters::Instance();
  param->SetAugerCascade(true);
  param->SetStepFunction(1., 1*CLHEP::mm);
  param->SetStepFunctionMuHad(1., 1*CLHEP::mm);

  //...

  // Decay
  RegisterPhysics(new G4DecayPhysics());
  // Radioactive decay
  // /process/had/rdm/thresholdForVeryLongDecayTime 1.0e+60 year
  RegisterPhysics(new BiasedRDPhysics());

  // // Hadron Elastic scattering
  // RegisterPhysics( new G4HadronElasticPhysics(verb) );

  // // Hadron Inelastic physics
  // RegisterPhysics( new G4HadronPhysicsFTFP_BERT(verb));
  // //RegisterPhysics( new G4HadronInelasticQBBC(verb));
  // ////RegisterPhysics( new G4HadronPhysicsINCLXX(verb));

  // // Ion Elastic scattering
  // RegisterPhysics( new G4IonElasticPhysics(verb));

  // // Ion Inelastic physics
  // RegisterPhysics( new G4IonPhysics(verb));
  // ////RegisterPhysics( new G4IonINCLXXPhysics(verb));

  // // Gamma-Nuclear Physics
  // G4EmExtraPhysics* gnuc = new G4EmExtraPhysics(verb);
  // gnuc->ElectroNuclear(false);
  // gnuc->MuonNuclear(false);
  // RegisterPhysics(gnuc);



  // =============================== HADR03 ====================

  //
  RegisterPhysics( new GammaNuclearPhysics("gamma"));
  //RegisterPhysics( new GammaNuclearPhysicsLEND("gamma"));
    // Radioactive decay
  RegisterPhysics(new G4RadioactiveDecayPhysics());

  // RegisterPhysics( new G4HadronElasticPhysicsHP(verb));
  RegisterPhysics( new G4HadronElasticPhysicsXS(verb));
  // Hadron Inelastic physics
  ////RegisterPhysics( new G4HadronPhysicsFTFP_BERT_HP(verb));
  RegisterPhysics( new G4HadronPhysicsQGSP_BIC_HP(verb));
  ////RegisterPhysics( new G4HadronPhysicsQGSP_BIC_AllHP(verb));
  ////RegisterPhysics( new G4HadronPhysicsQGSP_BIC(verb));
  ////RegisterPhysics( new G4HadronInelasticQBBC(verb));
  ////RegisterPhysics( new G4HadronPhysicsINCLXX(verb));
  ////RegisterPhysics( new G4HadronPhysicsShielding(verb));
  // Ion Elastic scattering
  RegisterPhysics( new G4IonElasticPhysics(verb));
  // Ion Inelastic physics
  //RegisterPhysics( new G4IonPhysicsXS(verb));
  RegisterPhysics( new G4IonPhysics(verb));

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PhysicsList::ConstructParticle()
{
  // minimal set of particles for EM physics and radioactive decay
  //
  G4EmBuilder::ConstructMinimalEmSet();


  G4BosonConstructor  pBosonConstructor;
  pBosonConstructor.ConstructParticle();

  G4LeptonConstructor pLeptonConstructor;
  pLeptonConstructor.ConstructParticle();

 G4MesonConstructor pMesonConstructor;
 pMesonConstructor.ConstructParticle();

  G4BaryonConstructor pBaryonConstructor;
  pBaryonConstructor.ConstructParticle();

  G4IonConstructor pIonConstructor;
  pIonConstructor.ConstructParticle();

  G4ShortLivedConstructor pShortLivedConstructor;
  pShortLivedConstructor.ConstructParticle();


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PhysicsList::SetCuts()
{
  SetCutValue(1*mm, "gamma");
  SetCutValue(1*mm, "proton");
  SetCutValue(1*mm, "e-");
  SetCutValue(1*mm, "e+");
  //   SetCutValue(1*mm, "ion"); // no effect
}
