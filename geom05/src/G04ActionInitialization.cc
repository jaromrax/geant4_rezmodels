/// \file G04ActionInitialization.cc
/// \brief Implementation of the G04ActionInitialization class

#include "G04ActionInitialization.hh"
#include "G04PrimaryGeneratorAction.hh"

// fo the user runaction
#include "G04RunAction.hh"



//#include "global_info.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G04ActionInitialization::G04ActionInitialization()
// : G4VUserActionInitialization()
{
  //  G4cout<<" _________________________________________________________________"<<G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G04ActionInitialization::~G04ActionInitialization()
{
  //    G4cout<<" ________destroy acvion initialization____________________________"<<G4endl;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void G04ActionInitialization::BuildForMaster() const
{
  //  RunAction* runAction = new RunAction(fDetector, primary );
  //  SetUserAction(runAction);

  //   RunAction* runAction = new RunAction(fDetector, nullptr);
  //  SetUserAction(runAction);


  G4cout<<" ______________buildFMas-actioninitialization_____________________"<<G4endl;
  // HAHA - THIS ALLOWS MERGING ROOT
  SetUserAction(new RunAction);
  G4cout<<" ______________buildFMas-actioninitialization_____________________"<<G4endl;
  G4AnalysisManager* fanalysisManager = G4AnalysisManager::Instance();
  // if (1==0){
  //   fanalysisManager->SetNtupleMerging(true);
  // }



}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void G04ActionInitialization::Build() const  // running for every thread
{
  //  G4cout<<" _______________Build in actioninitialization ____________________"<<G4endl;
    SetUserAction(new G04PrimaryGeneratorAction);  // this was here before !!!!

    //my attempt to add a  runaction based on standard G4...  => it somehow works !!!
    SetUserAction(new RunAction);

    //  G04EventAction* eventAction = new G04EventAction;
    //  SetUserAction(eventAction);

  //  SetUserAction(new G04SteppingAction(eventAction));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
