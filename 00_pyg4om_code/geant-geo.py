#!/usr/bin/env python3
"""
 for ((i=10;i<=40 ;i+=1));  do  sed -i 's|/gun/energy [0-9]* MeV|/gun/energy '"$i"' MeV|' setup.mac &&  ../00_pyg4om_code/geant-geo.py run ; done

FUNCTION:
  1. create geometry
  2. run test on geant4

IDEA:
  - find out where you are and prepare FOR _build folder
  - factorize geometry creation
  - save the geometry localy!, enhance/modify CMakefiles.txt to cointain it
  - check/create DISPOSABLE _build folder
  - do compile and run
  - collect the results

|       |   t | N  | hits   | fsize |                |
|-------+-----+----+--------+-------+----------------|
| 241Am | 301 | 1M | 187k   |   9.8 | alpha, cluster |
| 152Eu | 4.2 | 1M | 48.9k  |   8.8 |                |
| 22Na  | 4.4 | 1M | 92.1k  |   9.1 |                |
| 60Co  | 3.2 | 1M | 68.5   |   8.9 |                |
| 133Ba | 4.4 | 1M | 111.4k |   9.3 |                |
| 207Bi | 4.1 | 1M | 107.1k |   9.3 |                |
| 134Cs | 3.6 | 1M | 83.3k  |   9.1 |                |
| 88Yt  | 3.4 | 1M | 74k    |   9.0 |                |
|       |     |    |        |       |                |


 for ((i=10;i<=40 ;i+=2));  do  sed -i 's|/gun/energy [0-9]* MeV|/gun/energy '"$i"' MeV|' run.mac &&  ../00_pyg4om_code/geant-geo.py run ; done
# FROM geom06
 for ((i=10;i<=40 ;i+=1));  do  sed -i 's|/gun/energy [0-9]* MeV|/gun/energy '"$i"' MeV|' setup.mac &&  ../00_pyg4om_code/geant-geo.py run ; done

"""

import sys
import os
from console import fg,bg
import subprocess as sp
from contextlib import contextmanager
import datetime as dt
from fire import Fire
#
import ROOT
import pandas as pd
import numpy as np
import time
#
import pyg4ometry
import pyg4ometry.geant4 as _g4       # MATERIALS
from pyg4ometry.gdml import Auxiliary # SENSITIVE DETECTOR

import random
import math # for sqrt canvas
import sys
import datetime

print("i... hello, i am geometry...")
# https://geant4-userdoc.web.cern.ch/UsersGuides/ForApplicationDeveloper/html/Appendix/materialNames.html

PATH = "input.gdml"  # Just filename
#DATAFRAME = None
MYGEOMETRY = "???"


##########################################################################  FUNCTIONS system
##########################################################################  FUNCTIONS system
def move_file(fromdir,  moveme, dest):
    with cd(fromdir):
        CMD = f"mv {moveme} {dest}"
        CMD = CMD.strip()
        print(CMD)
        error = True
        try:
            grepOut = sp.call( CMD, shell=True) # sp.check_output( CMD, shell=True)
            error = False
        except sp.CalledProcessError as grepexc:
            error = True
            print("error code", grepexc.returncode, grepexc.output)
        if error:
            print(bg.red, fg.white," XXXXXXXXXXXXXXXXXXXXXXXXXXXX {CMD}  ERROR XXXXXXXXX", bg.default, fg.default)
        return None


def remove_file(removeme):
    CMD = f"rm {removeme}"
    CMD = CMD.strip()
    print(CMD)
    error = True
    try:
        grepOut = sp.call( CMD, shell=True) # sp.check_output( CMD, shell=True)
        error = False
    except sp.CalledProcessError as grepexc:
        error = True
        print("error code", grepexc.returncode, grepexc.output)
    if error:
        print(bg.red, fg.white," XXXXXXXXXXXXXXXXXXXXXXXXXXXX RM OUTPUT.CSV ERROR XXXXXXXXX", bg.default, fg.default)
    return None


def extract_one_line(fromfile, tag):
    with open(fromfile, 'r') as file:
        for line in file:
            if tag in line and not line.lstrip().startswith('#'):
                return line.replace(tag,"").rstrip()
    return None


def add_log_row(txt):
    with open("../11running.log","a") as f:
        now = dt.datetime.now()
        now = str(now)[:-5]
        f.write( f"{now}   {txt}")
        f.write('\n')
    print(f"{fg.cyan}L... {txt}{fg.default}")



@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

# ===============================================
def get_build_folder():
    # check for
    chkfiles = ["CMakeLists.txt","vis.mac","src","include"]
    error = False
    for i in chkfiles:
        if not os.path.exists(i):
            error = True
            print(bg.red, fg.white, f"i... FILE DOESNOT EXIST {i}  xxxxxxxxxxxx", bg.default, fg.default )
    if error:
        return None,None

    curr = os.getcwd()
    base = os.path.split( curr )[0]
    cdir = os.path.split( curr )[1]
    build = base+"/build_"+cdir+"_build"
    # if os.path.exists(build):
    if os.path.isdir(build):
        pass #print("i... build folder exists: ", build)
    else:
        print(fg.red,"i... build folder created NOW: ", build, fg.default)
        os.makedirs(build)
    return build, cdir # cdir = project name


# ===============================================
def BUILD_ME():
    BUILD,orig = get_build_folder()
    error = False
    with cd(BUILD):
        CMD = f"cmake ../{orig}"
        try:
            grepOut = sp.check_output( CMD, shell=True)
        except sp.CalledProcessError as grepexc:
            error = True
            print("error code", grepexc.returncode, grepexc.output)
    if error:
        print(bg.red, fg.white," XXXXXXXXXXXXXXXXXXXXXXXXXXXX CONFIG error XXXXXXXXXXXX", bg.default, fg.default)
        return False
    else:
        print(fg.green,"___________________________________________________ CONFIG DONE", fg.default)
    #
    #
    with cd(BUILD):
        CMD = f"cmake --build . -j {os.cpu_count()}"
        print(CMD)
        try:
            grepOut = sp.check_output( CMD, shell=True)
        except sp.CalledProcessError as grepexc:
            error = True
            print("error code", grepexc.returncode, grepexc.output)
    if error:
        print(bg.red, fg.white," XXXXXXXXXXXXXXXXXXXXXXXXXXXX COMPILATION error XXXXXXXXXXXX", bg.default, fg.default)
        return False
    else:
        print(fg.green,"___________________________________________________ COMPILATION DONE", fg.default)

    return True


# ===============================================
def RUN_ME( geometryfile="" , macro="" ):
    """

    """
    BUILD,orig = get_build_folder()
    error = False
    now = dt.datetime.now()
    with cd(BUILD):
        # check existence of outputs
        #ocvs = [x for x in os.listdir() if x.find('output')==0 and x.find('.csv')==len(x)-4]
        ocvs = [x for x in os.listdir() if x.find('output')==0 and x.find('.root')==len(x)-5]
        if len(ocvs)>0:
            print(ocvs)
            print(bg.red, fg.white," XXXXXXXXXXXXXXXXXXXXXXXXXXXX output.root file(s) present ==>> NO RUN  XXXXXXXXXXXX", bg.default, fg.default)
            sys.exit(1)
            return False
        #
        #
        CMD = ""
        CORES = os.cpu_count()

        # CORES = 1 # FOR DE
        if macro == "vis" or macro == "vis.mac" or macro == "skip" or macro == "skip.mac":
            CMD = f"./gdml_det "+ geometryfile + " " + macro
        else:
            CMD = f"G4FORCENUMBEROFTHREADS={CORES} ./gdml_det "+ geometryfile + " " + macro
        CMD = CMD.strip()
        print(CMD)
        try:
            grepOut = sp.call( CMD, shell=True) # sp.check_output( CMD, shell=True)
        except sp.CalledProcessError as grepexc:
            # PROBLEM, ERROR IS NOT DETECTED>>>>>>>>>>>>>>>>>>>>
            error = True
            print("error code", grepexc.returncode, grepexc.output)
    if error:
        print(bg.red, fg.white," XXXXXXXXXXXXXXXXXXXXXXXXXXXX RUNTIME  error XXXXXXXXXXXX", bg.default, fg.default)
        return False
    else:
        print(fg.green,"___________________________________________________ RUN      DONE", fg.default)
    print(f"i... ... time spent {dt.datetime.now()-now} ")
    print()
    return True


# ===============================================
def collect_me( geometryfile="" ):
    """
    ONLY WITH CSV
    cat output*csv > merged.csv
    return filename or None
    """
    BUILD,orig = get_build_folder()
    error = False
    now = dt.datetime.now().strftime("%Y%m%d_%H%M%S")
    MERGEDFILE = f"o_{orig}_merged_{geometryfile}_{now}.csv"

    with cd(BUILD):
        ocvs = [x for x in os.listdir() if x.find('output')==0 and x.find('.csv')==len(x)-4]
        if len(ocvs)==0:
            print(fg.green,"___________________________________________________ COLLECTION empty", fg.default)
            return None

        # ---------------  straight way to join ---------------------
        #ocvs = " ".join(ocvs)
        #CMD = f"cat {ocvs} > ../{MERGEDFILE}"
        #CMD = CMD.strip()
        # -----------------------------------------------------------
        ni = 0
        for i in ocvs:
            if ni==0: # fist one has column names
                CMD = f"cat {i} > ../{MERGEDFILE}"
            else:
                CMD = f"cat {i} | grep -v \# >> ../{MERGEDFILE}"
            CMD = CMD.strip()
            print(CMD)
            try:
                grepOut = sp.call( CMD, shell=True) #
            except sp.CalledProcessError as grepexc:
                error = True
                print("error code", grepexc.returncode, grepexc.output)
            ni+=1
        #------------------ all should be
    if error:
        print(bg.red, fg.white," XXXXXXXXXXXXXXXXXXXXXXXXXXXX COLLECTION error XXXXXXXXXXXX", bg.default, fg.default)
        return None
    else:
        print(fg.green,"___________________________________________________ COLLECTION DONE", fg.default)


    with cd(BUILD):
        Rcvs = [x for x in os.listdir() if x.find('output')==0 and x.find('.csv')==len(x)-4]
        if len(Rcvs)>0:
            Rcvs = " ".join(Rcvs)
            CMD = f"rm {Rcvs}"
        else:
            CMD = "echo RM PROBLEM...NO outputcsv"
        CMD = CMD.strip()
        print(CMD)
        try:
            grepOut = sp.call( CMD, shell=True) # sp.check_output( CMD, shell=True)
        except sp.CalledProcessError as grepexc:
            error = True
            print("error code", grepexc.returncode, grepexc.output)
    if error:
        print(bg.red, fg.white," XXXXXXXXXXXXXXXXXXXXXXXXXXXX RM OUTPUT.CSV ERROR XXXXXXXXX", bg.default, fg.default)
        return None
    else:
        print(fg.green,"___________________________________________________ CLEANUP DONE", fg.default)

    return os.path.split(BUILD)[0]+"/"+MERGEDFILE

##########################################################################  FUNCTIONS root
##########################################################################  FUNCTIONS root




def histo_me_from_np(hname, data):
    """
    numpy to root histo
    """
    if type(data[0]) == str:
        print("X... not processing this column - string")
        return None

    now = dt.datetime.now()
    #print(f"i... ... time spent {dt.datetime.now()-now} ")
    print( type(data[0]) , type(data[1]) , type(data) )
    data = data*1000 # converting MeV to keV
    emax = np.max(data)
    print(" ... emax = ", emax)
    print("_________________HISTO == ",hname)

    nmax=[1,5,10,50,100,500,1000,5000,10000,50000,100000,500000]
    # set maximum
    for imax in nmax:
        # print(f" ... {emax} < {imax} ?" )
        if emax<imax:
            emax = imax
            break

####################################################################
#
# this is the correct? way to get TH1 binning. It is consistent with ROOT
#  where TH1F *h = new TH1F("A","A", 10000,0,10);h->Fill(3.022);
#     for (int i=3020;i<3029; i++){ float v=h->GetBinContent(i); printf("%d %f\n", i,v);}
# GIVES channel 3022;   for 10010 bins it is clear.
####################################################################
    NBINS = 10000
    # max value is 3.022, check the bin;;  +1 no, -1 no
    now = dt.datetime.now()
    # I need to match with TH1;(0, emax, num=NBINS+1) THIS  really gives NBINS   bins, edges 0 and emax
    bins = np.linspace(0, emax, num=NBINS+1)
    #
    nphist = np.histogram(data, bins, range=( 0.0, emax ) )
    print(f"i... ... time spent {dt.datetime.now()-now} ")
    print(nphist[1])
    print("                 >>>> edges:  ",  len(nphist[1]) ," bins:" , len(nphist[0])  ) # -====

    # ================ HISTOGRAM FILL ===============
    hist = ROOT.TH1F(hname, f'{hname}...;E [keV]', NBINS, 0, emax) # BIN 0 I underflow
    ihi = 0
    for i in range(NBINS):  # from 0 to last bin
        hist.SetBinContent( i+1, nphist[0][i] )  # BIN 0  IS UNDERF
    hist.SetEntries( len(data) )
    return hist


# def load_dataframe(infile):
#     """
#     read csv with pandas, play with column names
#     """
#     #global DATAFRAME
#     print(" ... reading csv", infile)
#     now = dt.datetime.now()
#     with open(infile, 'r') as file:
#         lines = file.readlines()
#         colnames = [line.split()[2] for line in lines if line.startswith('#column')]

#     DATAFRAME = pd.read_csv(infile, header=None,comment="#", names=colnames)
#     #df = pd.read_csv(infile, comment="#")
#     #colnames = df.columns.tolist()
#     print("                ____________________colnames________________")
#     print(colnames)
#     newcols=colnames
#     #newcols=[]
#     #for i in range(ncolumns):
#     #    newcols.append(f"e{i}")
#     newcols.pop(0)
#     newcols.insert(0,'n')
#     DATAFRAME.columns = newcols
#     print( DATAFRAME.head() )
#     return DATAFRAME


def load_dataframe(infile, geantfile = True):
    """
    ONLY FOR CSV
    read csv with pandas
    -  geant merged csv  file needs different treatment than  dataframe to_csv created
    """
    #global DATAFRAME
    print(" ... reading csv", infile)
    now = dt.datetime.now()
    if geantfile:
        with open(infile, 'r') as file:
            lines = file.readlines()
            colnames = [line.split()[2] for line in lines if line.startswith('#column')]
            print(colnames)
            newcols=colnames
            #newcols=[]
            #for i in range(ncolumns):
            #    newcols.append(f"e{i}")
            newcols.pop(0)
            newcols.insert(0,'n')
            DATAFRAME.columns = newcols
            DATAFRAME = pd.read_csv(infile, header=None,comment="#", names=colnames)
    else:
        DATAFRAME = pd.read_csv(infile, comment="#", low_memory=False)
    #df = pd.read_csv(infile, comment="#")
    #colnames = df.columns.tolist()
    print( DATAFRAME.head() )
    print("                ____________________ DF in memory ________________")
    return DATAFRAME


def newname(infile ,  MYGEOMETRY, Lene, Levt, Lpar, timetag=None):
    base,ext = os.path.splitext( infile )
    path,fname = os.path.split(base)
    print(f"i...  path  /{path}/")
    print(f"i...  fname /{fname}/")
    print(f"i...  ext   /{ext}/")

    tita = "X"
    if timetag is not None:
        print(type(timetag))
        if type(timetag) == datetime.datetime:
            tita = timetag.strftime("%Y%m%d_%H%M%S")

    # ------------_g _E _n _p can serve as tags
    if len(path)==0:
        new = f"o{fname}_t{tita}_g{MYGEOMETRY}_E{Lene}_n{Levt}_p{Lpar}{ext}"
    else:
        new = f"{path}/o{fname}_t{tita}_g{MYGEOMETRY}_E{Lene}_n{Levt}_p{Lpar}{ext}"


    new = new.replace(" ","")
    new = new.replace("_merged_","")
    print(f"i... {fg.orange} saving {fg.default} {new}")
    return new

def save_dataframe(df, infile ,  MYGEOMETRY, Lene, Levt, Lpar):
    """
    ONLY FOR CSV
    """
    filenew = newname(infile)
    df.to_csv(filenew, index=False) # do not seva index
    return filenew


def histo_me( df ):
    """
    ONLY WITH CSV
    read csv with pandas, (may generate root tree)
    create histo with np,;  display in canvas
    """
    #global DATAFRAME
    # # From MeV to keV ================
    # print(" ... reading csv", infile)
    now = dt.datetime.now()
    # with open(infile, 'r') as file:
    #     lines = file.readlines()
    #     colnames = [line.split()[2] for line in lines if line.startswith('#column')]

    # df = pd.read_csv(infile, header=None,comment="#", names=colnames)
    # #df = pd.read_csv(infile, comment="#")
    # #colnames = df.columns.tolist()
    # print("                ____________________colnames________________")
    # print(colnames)



    # #print(df.head() )
    # #ncolumns = len(df.columns)
    # newcols=colnames
    # #for i in range(ncolumns):
    # #    newcols.append(f"e{i}")
    # newcols.pop(0)
    # newcols.insert(0,'n')
    # df.columns = newcols
    # print( df.head() )

    totevents = len(df)
    interest = df[~df[df.columns[-1]].str.contains("none")]
    intevents = len( interest)
    print(f"i...    Total: {totevents}  triton: {intevents}   ")
    print("                ____________________colnames________________")



    # What is this????  Nonzeroes?   With nonenonenone  6-2=4  doesntwork
    #df = df[np.count_nonzero(df.values, axis=1) > len(df.columns)-2]
    #df.drop(df[df.e == 0].index, inplace = True)

    # =======================================================
    #    I only multiply by 1000 to get keV ;   possilble to create gauss random.normal

    # -------- multiply by 1000  OR add some gaussian
    #for a in newcols:
    #    if a=="n":continue
    #    print("******************* column to 1000x",a)
    #    df[a] = df[a]*1000
        ## df['e'] = df['e']*1000  + random.gauss(0, 2.35 )# + 2.35*df['e']/1.3 )
        #df[a] = df[a].apply(lambda x: 1000*x + np.random.normal(0, 1.4 + 0.6*1000*x/1300 )/2.35 )

    #df['e'] = random.gauss( df['e']*1000,  12.35 )# + 2.35*df['e']/1.3 )
    print(f"i... ... time spent {dt.datetime.now()-now} ")


    if (1==0):  # ==========================  problems on new root =======
        now = dt.datetime.now()
        print("___________ CREATING ROOT FILE ___________ ")
        datadic = {key: df[key].values for key in df.columns}
        rdf = ROOT.RDF.MakeNumpyDataFrame(datadic)
        rdf.Snapshot("sim", "myFile.root" );
        print("___________ CREATed  ROOT FILE ___________ ")
        print(f"i... ... time spent {dt.datetime.now()-now} ")


    print("_________________")
    print( f"... columns: {len(df.columns)} ")
    print( f"...  length: {len(df)}        to_numpy..................")



    # ======================  create histogram
    allhi=[]
    for a in df.columns:
        if a=="n":continue
        data = df[a].to_numpy()
        hist=histo_me_from_np( a, data )
        if hist is None:
            print("X...  Not Processing this column ... ", type(data[0])  )
            continue
        print("i... hist done", hist)
        hist.Print()
        # print( hist.GetXaxis().GetXmin() , hist.GetXaxis().GetXmax() , " centers of 1 and NBINS"  ,hist.GetBinCenter(1), hist.GetBinCenter(NBINS)   )
        allhi.append(hist)

    ROOT.gStyle.SetOptStat(1111111)

    #    for value in data:  # uses numpy
    #        hist.Fill(value)
    if len(allhi)>1:
        c1 = ROOT.TCanvas( "c1")
        # works to 16+
        t=len(allhi)
        wid, hig =  math.floor(math.sqrt(t)) , math.floor(t/math.floor(math.sqrt(t))+0.75)
        c1.Divide( wid, hig )
        for i in range(len(allhi)):
            c1.cd(i+1)
            allhi[i].Draw()
            ROOT.gPad.SetLogy()
    else:
        allhi[0].Draw()

    ROOT.gPad.SetLogy()
    ROOT.gPad.Modified()
    ROOT.gPad.Update()
    time.sleep(3)
    #print("K... press enter ")
    #input()


##########################################################################  FUNCTIONS geometry
##########################################################################  FUNCTIONS geometry

def view( world_l ):
    # visualise geometry
    print(" ... visualization ..., press enter to continue")
    v = pyg4ometry.visualisation.VtkViewer()
    v.addLogicalVolume(world_l)
    v.addAxes(20)
    v.view()
    #input()


def make_world( REG , air,  dim=(1000,1000,1000) ):
    """
    world is always the same
    """
    # world solid and logical
    vacuum = _g4.MaterialPredefined("G4_Galactic")

    air_c = _g4.MaterialCompound("air",1.290e-3,2,REG)
    n_ele = _g4.ElementSimple("nitrogen","N",7,14.01)
    o_ele = _g4.ElementSimple("oxygen","O",8,16.0)
    air_c.add_element_massfraction(n_ele,0.7)
    air_c.add_element_massfraction(o_ele,0.3)

    ws = pyg4ometry.geant4.solid.Box("ws", dim[0], dim[1], dim[2], REG)
    if air == True:
        wl = pyg4ometry.geant4.LogicalVolume(ws, air_c, "world_l", REG)
    else:
        wl = pyg4ometry.geant4.LogicalVolume(ws, vacuum, "world_l", REG)
    REG.setWorld(wl.name)
    return wl





#########################################################################################
#########################################################################################
#             MAIN
#########################################################################################
#########################################################################################
def main( macro = "vis" , onlyview = False):
    """
    -o  ... use viewer only
    vis  ...  run vis.mac
    run  ...  run run.mac
    """
    global MYGEOMETRY
    # REG ============= MAIN OBJECT CONTAING THE MODEL ===============================
    REG = pyg4ometry.geant4.Registry()    # #1 --  registry to store gdml data
    # vacuum = _g4.MaterialPredefined("G4_Galactic")
    #iron = _g4.MaterialPredefined("G4_Fe",reg)
    # hpge = _g4.MaterialPredefined("G4_Ge",reg)    # print(iron.name)
    #berylium = _g4.MaterialPredefined("G4_Be",reg)    # print(iron.name)
    #alu = _g4.MaterialPredefined("G4_Al",reg)


# =========================================================  materials can remain over models======
    hpge_c = _g4.MaterialCompound("MatHPGe", 5.327,1,REG)
    ge_ele = _g4.ElementSimple("Germanium","Ge", 32, 72.630)
    hpge_c.add_element_massfraction(ge_ele,1)

    be_c = _g4.MaterialCompound("MatBerylium", 1.85,1,REG)
    be_ele = _g4.ElementSimple("Berylium","Be", 4, 9)
    be_c.add_element_massfraction(be_ele,1)

    li_c = _g4.MaterialCompound("MatLithium", 0.5334, 1, REG)
    li_ele = _g4.ElementSimple("Lithium","Li", 3, 6.94)
    li_c.add_element_massfraction(li_ele,1)

    al_c = _g4.MaterialCompound("MatAluminium", 2.699, 1, REG)
    al_ele = _g4.ElementSimple("Aluminium","Al", 13, 26.981 )
    al_c.add_element_massfraction(al_ele,1)

    cu_c = _g4.MaterialCompound("MatCopper", 8.935, 1, REG)
    cu_ele = _g4.ElementSimple("Copper","Cu", 29, 44.01 )
    cu_c.add_element_massfraction(cu_ele,1)

    h2o_c = _g4.MaterialCompound("MatWater", 1.00, 2, REG)
    h_ele = _g4.ElementSimple("Hydrogen","H", 1, 1.0078 )
    o_ele = _g4.ElementSimple("Oxygen","O", 8, 16.00 )
    h2o_c.add_element_natoms(h_ele,2)
    h2o_c.add_element_natoms(o_ele,1)


    vacuum = _g4.MaterialPredefined("G4_Galactic")

    world_l = make_world( REG, air=False )  # ============= CALL THE WORLD NOW

    # ####################################################################
    # # box placed at origin
    # ####################################################################
    # b1 = pyg4ometry.geant4.solid.Box("b1", 250, 130, 130, reg)
    # # logical = solid + material + NAME intoo REG
    # b1_l = pyg4ometry.geant4.LogicalVolume(b1, hpge, "b1_l", reg) #// THIS CAN BE TRACKED
    # # make it sensitive
    # aa = Auxiliary("SensDet", "Tracker")
    # b1_l.addAuxiliaryInfo(aa)  #'<auxiliary auxtype="SensDet" auxvalue="Tracker"/>')



    ####################################################################
    # TUBE placed at origin
    ####################################################################
    MYGEOMETRY = "natLi3.5onAl1"

    c1_dia = 25
    c1_thickness = 0.5
    c1 = pyg4ometry.geant4.solid.Tubs("c1", 0, c1_dia/2, c1_thickness, 0, 360, REG, lunit='mm', aunit='deg')
    c1_l = pyg4ometry.geant4.LogicalVolume(c1, li_c, "c1_l", REG) #// THIS CAN BE TRACKED
    li_col = (0.8,0.8,1)
    # make it sensitive
    c1_l.addAuxiliaryInfo( Auxiliary("SensDet", "Tracker") )  #'<auxiliary auxtype="SensDet" auxvalue="Tracker"/>')


    c2_dia = c1_dia
    c2_thickness = 0.025
    c2 = pyg4ometry.geant4.solid.Tubs("c2", 0, c2_dia/2, c2_thickness, 0, 360, REG, lunit='mm', aunit='deg')
    c2_l = pyg4ometry.geant4.LogicalVolume(c2, cu_c, "c2_l", REG) #// THIS CAN BE TRACKED
    cu_col = (1,0,0)
    # make it sensitive
    c2_l.addAuxiliaryInfo( Auxiliary("SensDet", "Tracker") )  #'<auxiliary auxtype="SensDet" auxvalue="Tracker"/>')

    # Al plate
    c3_dia = c1_dia
    c3_thickness = 5
    c3 = pyg4ometry.geant4.solid.Tubs("c3" , 0, c3_dia/2, c3_thickness, 0, 360, REG, lunit='mm', aunit='deg')
    c3_l = pyg4ometry.geant4.LogicalVolume(c3, al_c, "c3_l", REG) #// THIS CAN BE TRACKED
    al_col = (0.8,0.8,0.8)
    c3_l.addAuxiliaryInfo( Auxiliary("SensDet", "Tracker") )  #'<auxiliary auxtype="SensDet" auxvalue="Tracker"/>')


    # Water barrel
    c4_dia = 500
    c4_thickness = 500
    c4 = pyg4ometry.geant4.solid.Tubs("c4" , 0, c4_dia/2, c4_thickness, 0, 360, REG, lunit='mm', aunit='deg')
    c4_l = pyg4ometry.geant4.LogicalVolume(c4, h2o_c, "c4_l", REG) #// THIS CAN BE TRACKED
    h2o_col = (0,0,1)
    c4_l.addAuxiliaryInfo( Auxiliary("SensDet", "Tracker") )  #'<auxiliary auxtype="SensDet" auxvalue="Tracker"/>')



    # ========================================================= PLACEMENT======================
    # ========================================================= PLACEMENT======================
    # ========================================================= PLACEMENT======================
    foils = {}
    distance_face = 10
    distance = distance_face

    distance = distance + c1_thickness/2
    foils['f1'] = pyg4ometry.geant4.PhysicalVolume([0, 0, 0], [0, 0,  distance], c1_l, "li1", world_l, REG, copyNumber=1)
    foils['f1'].visOptions.colour = li_col
    foils['f1'].visOptions.alpha = 1
    distance = distance + c1_thickness/2

    distance = distance + c2_thickness/2
    foils['f2'] = pyg4ometry.geant4.PhysicalVolume([0, 0, 0], [0, 0,  distance], c2_l, "cu2", world_l, REG, copyNumber=2 )
    foils['f2'].visOptions.colour = cu_col
    foils['f2'].visOptions.alpha = 1
    distance = distance + c2_thickness/2

    distance = distance + c1_thickness/2
    foils['f3'] = pyg4ometry.geant4.PhysicalVolume([0, 0, 0], [0, 0,  distance], c1_l, "li3", world_l, REG, copyNumber=3 )
    foils['f3'].visOptions.colour = li_col
    foils['f3'].visOptions.alpha = 1
    distance = distance + c1_thickness/2

    distance = distance + c2_thickness/2
    foils['f4'] = pyg4ometry.geant4.PhysicalVolume([0, 0, 0], [0, 0,  distance], c2_l, "cu4", world_l, REG, copyNumber=4 )
    foils['f4'].visOptions.colour = cu_col
    foils['f4'].visOptions.alpha = 1
    distance = distance + c2_thickness/2

    distance = distance + c1_thickness/2
    foils['f5'] = pyg4ometry.geant4.PhysicalVolume([0, 0, 0], [0, 0,  distance], c1_l, "li5", world_l, REG, copyNumber=5 )
    foils['f5'].visOptions.colour = li_col
    foils['f5'].visOptions.alpha = 1
    distance = distance + c1_thickness/2


    distance = distance + c3_thickness/2
    foils['f6'] = pyg4ometry.geant4.PhysicalVolume([0, 0, 0], [0, 0,  distance], c3_l, "al6", world_l, REG, copyNumber=6 )
    foils['f6'].visOptions.colour = al_col
    foils['f6'].visOptions.alpha = 1
    distance = distance + c3_thickness/2


    distance = distance + 50
    distance = distance + c4_thickness/2
    foils['f7'] = pyg4ometry.geant4.PhysicalVolume([0, 0, 0], [0, 0,  distance], c4_l, "h2o7", world_l, REG, copyNumber=7)
    foils['f7'].visOptions.colour = h2o_col
    foils['f7'].visOptions.alpha = 0.5
    distance = distance_face + c4_thickness/2


    if "geom05" == "":
        ####################################################################
        # TUBE placed at origin
        ####################################################################
        # hpge detector itself
        c_dia = 100
        c_z = 100
        c1 = pyg4ometry.geant4.solid.Tubs("c1", 0, c_dia/2, c_z, 0, 360, REG, lunit='mm', aunit='deg')
        c1_l = pyg4ometry.geant4.LogicalVolume(c1, li_c, "c1_l", REG) #// THIS CAN BE TRACKED
        # make it sensitive
        aac = Auxiliary("SensDet", "Tracker")
        c1_l.addAuxiliaryInfo(aac)  #'<auxiliary auxtype="SensDet" auxvalue="Tracker"/>')

        # HOLLOW TUBE -------------------------------------
        dr = 3
        c2 = pyg4ometry.geant4.solid.Tubs("c2", c_dia/2+dr-1.5, c_dia/2+dr, c_z+dr*2, 0, 360, REG, lunit='mm', aunit='deg')
        c2_l = pyg4ometry.geant4.LogicalVolume(c2, al_c, "c2_l", REG) #// THIS CAN BE TRACKED

        # Al plate
        thick = 0.5
        c3 = pyg4ometry.geant4.solid.Tubs("c3" , 0, c_dia/2+dr, thick, 0, 360, REG, lunit='mm', aunit='deg')
        c3_l = pyg4ometry.geant4.LogicalVolume(c3, al_c, "c3_l", REG) #// THIS CAN BE TRACKED
        c3_l.addAuxiliaryInfo( Auxiliary("SensDet", "Tracker") )  #'<auxiliary auxtype="SensDet" auxvalue="Tracker"/>')

        ######################## PLACE INTO WORLD ############################
        #   colour! will work in local visu, not in geant...
        ##############################################################
        # position, rotation, logical, NAME SEEN IN geant, mather volume (Name seen in geant too)
        # pyg4ometry.geant4.PhysicalVolume([0, 0, 0], [0, 0, 0], b1_l, "b1_p", world_l, reg)
        dist_det = 100
        c1_p = pyg4ometry.geant4.PhysicalVolume([0, 0, 0], [0, 0, c_z/2 + dist_det], c1_l, "c1_p", world_l, REG )
        c1_p.visOptions.colour = (1,0,0)
        c1_p.visOptions.alpha = 1

        c2_p = pyg4ometry.geant4.PhysicalVolume([0, 0, 0], [0, 0, (c_z)/2 + dist_det], c2_l, "c2_p", world_l, REG)
        c2_p.visOptions.colour = (1,1,0)
        c2_p.visOptions.alpha = 0.5

        #c3_p = pyg4ometry.geant4.PhysicalVolume([0, 0, 0], [0, 0,  dist_det-dr], c3_l, "c3_p", world_l, reg)
        #c3_p.visOptions.colour = (0,1,1)
        #c3_p.visOptions.alpha = 0.5


    # #################################################################################################################
    # #################################################################################################################
    # #################################################################################################################
    if onlyview:   # =================    -o parameter inline, view and QUIT =======================
        view(world_l)
        sys.exit(0)


    add_log_row(f"_____________________________________________________________________")
    print(" ... writing  ...", PATH)
    w = pyg4ometry.gdml.Writer()
    print(" ... adding detector  ...", PATH)
    w.addDetector(REG)
    print(" ... writing   ...", PATH)
    w.write(PATH)
    add_log_row(f"geometry created @ {os.getcwd()} / {PATH}")
    add_log_row(f"******** {MYGEOMETRY} ********")
    #____________________________________________________________________ Model is writter to input file

    # ..............

    #____________________________________________________________________ commandline macro (vis or run)
    macrox = macro
    merged_file = None

    if macrox.find(".mac") != len(macrox)-3:
        macrox = macrox + ".mac"
    # -----------  BUILD HERE --------------------------------   ********
    Lene = ""
    Lpar = ""
    Levt = ""
    if BUILD_ME():
        add_log_row(f"code built and running with /{macrox}/ ...")
        if macrox == "run.mac":
            Lene = extract_one_line(macrox, "/gun/energy")
            if Lene is None:
                Lene = extract_one_line("setup.mac", "/gun/energy")

            Lpar = extract_one_line(macrox, "/gun/particle")
            if Lpar is None:
                Lpar = extract_one_line("setup.mac", "/gun/particle")

            Levt = extract_one_line(macrox, "/run/beamOn")
            if Levt is None:
                Levt = extract_one_line("setup.mac", "/run/beamOn")
            Levt = int(Levt.strip())

            add_log_row(f"... particle: {Lpar} ...")
            add_log_row(f"... Energy  : {Lene} ...")
            add_log_row(f"... Events  : {int(Levt/1000)} x 1000 ...")


        now = dt.datetime.now()
        started = dt.datetime.now()
        if RUN_ME( PATH, macrox ): # --------------------- RUN HERE =------------------------ ******
            add_log_row(f"...run finished ................ #  {dt.datetime.now() - now}" )
            #sys.exit(0)
            if "csv" == "xxx":
                merged_file = collect_me()
                add_log_row(f"output file {merged_file}")
            else: # ROOT .../ move up
                # collect_me provided TimeTagged file
                #now = dt.datetime.now().strftime("%Y%m%d_%H%M%S")
                #MERGEDFILE = f"o_{orig}_merged_{geometryfile}_{now}.csv"
                merged_file = "output.root"
                BUILD,orig = get_build_folder()
                move_file( BUILD,  merged_file, "../")
                pass
                # ----- when merged, nothing to delete
                #Rroot = [x for x in os.listdir() if x.find('output')==0 and x.find('.csv')==len(x)-4]
                #print(f"i... delete: {Rroot}")



    # =========================================  visualize macros only....
    df = None
    if macrox not in ["vis.mac","skip.mac"]:
        if merged_file is not None:
            if "csv" == "xxx":
                df = load_dataframe(merged_file)
                outfile = save_dataframe(df, merged_file, MYGEOMETRY, Lene, Levt, Lpar)
                add_log_row(f"...csv dataframe saved {outfile}")
                add_log_row(f"... rows   = {len(df)}")
                tgt = df[~df['target'].str.contains("none")]
                add_log_row(f"... targ   = {len(tgt)}")
                tg2 = df[~df['firstv'].str.contains("none")]
                add_log_row(f"... 1stvol = {len(tg2)}")
                histo_me(df)
                add_log_row(f"Removing {merged_file}")
                remove_file(merged_file)
            else: # ROOT FILE
                #MERGEDFILE = f"o_{orig}_merged_{geometryfile}_{now}.csv"
                fnewname = newname( merged_file ,  MYGEOMETRY, Lene, Levt, Lpar, timetag = started )
                add_log_row(f"renaming  {merged_file} -->>  {fnewname}")
                move_file("..", merged_file, fnewname )


        else:
            print("X... no merged_file")


    add_log_row(f"_______________________________________________________________________")
    return 0
    # make a quick bdsim job for the one component in a beam line
    # w.writeGmadTester('file.gmad', 'file.gdml')



if __name__ == "__main__":
    Fire(main)
    sys.exit(0)
print("#... this was run from EMACS ...to run from python: C-c C-p ; C-c C-c")
os.chdir("../geom03_gdml")
print(os.getcwd() )
main( onlyview = True)
