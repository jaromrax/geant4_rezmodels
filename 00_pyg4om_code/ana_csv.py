#!/usr/bin/env python3

from fire import Fire

import pandas as pd
import numpy as np
import datetime as dt
import os

# Function to process a single CSV file
def process_file(filepath):
    df = pd.read_csv(filepath)

    print("###########")
    print("----------------------------------------- leavev not None")
    dfi = df[df['leavev'] != 'none']
    print(dfi)
    print("---------------------------------------------------------")


    conditions = [
        (df['firstv'] == 'c1_p') & (df['stopv'] == 'c1_p'),
        (df['firstv'] == 'c1_p') & (df['stopv'] == 'c3_p'),
        (df['firstv'] == 'c3_p') & (df['stopv'] == 'c3_p'),
        (df['firstv'] == 'c3_p') & (df['stopv'] == 'c1_p'),
        # Add more conditions as needed
    ]
    counts = [df[condition].shape[0] for condition in conditions]
    return counts


def load_dataframe(infile, geantfile = True):
    """
    read csv with pandas, play with column names
    """
    #global DATAFRAME
    print(" ... reading csv", infile)
    now = dt.datetime.now()
    if geantfile:
        with open(infile, 'r') as file:
            lines = file.readlines()
            colnames = [line.split()[2] for line in lines if line.startswith('#column')]
            print(colnames)
            newcols=colnames
            #newcols=[]
            #for i in range(ncolumns):
            #    newcols.append(f"e{i}")
            newcols.pop(0)
            newcols.insert(0,'n')
            DATAFRAME.columns = newcols
            DATAFRAME = pd.read_csv(infile, header=None,comment="#", names=colnames)
    else:
        DATAFRAME = pd.read_csv(infile, comment="#", low_memory=False)
    #df = pd.read_csv(infile, comment="#")
    #colnames = df.columns.tolist()
    print( DATAFRAME.head() )
    print("                ____________________ DF in memory ________________")
    return DATAFRAME


###########################################################################################

def main(csvfile=None):
    print("#"*70)
    print(" "*20, "ANALYSIS")
    print("#"*70)
    # df = load_dataframe(csvfile, geantfile = False)


    # dfi = df[~df[df.columns[-1]].str.contains("none")]
    # print(dfi)
    # print("----------------------------------------- LAST not None")

    # print(df.columns)


    # print("###########")
    # dfj = df[df['stopv'] != 'none']
    # print(dfj)
    # print("----------------------------------------- stopv  not None")


    # print("###########")
    # dfi = df[df['leavev'] != 'none']
    # print(dfi)
    # print("----------------------------------------- leavev not None")


    # List to store results from all files
    results = []

    # Directory containing CSV files
    directory = '.'

    # Iterate over CSV files in the directory
    for filename in os.listdir(directory):
        if filename.endswith('.csv'):
            filepath = os.path.join(directory, filename)
            print(f"i... processing {filepath}")
            file_counts = process_file(filepath)
            results.append([filepath]+file_counts)

    # Aggregate results into a final report
    report_df = pd.DataFrame(results, columns=['file','c1_p & c1_p', 'c1_p & c3_p','c3_p & c3_p', 'c3_p & c1_p'])

    print("---------------------------------------REPORT---------------------")
    print( report_df)

if __name__=="__main__":
    Fire(main)
