
#include "G04SensitiveDetector.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

// get track
#include "G4RunManager.hh"
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4Gamma.hh"
#include "G4Neutron.hh"
#include "G4Proton.hh"
// not needed #include "G4Ions.hh"


#include "G4AnalysisManager.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// my:  nanosecods
#include "G4SystemOfUnits.hh"
// i need to know vis or run
#include "global_info.hh"


G04SensitiveDetector::G04SensitiveDetector(const G4String& name)
  : G4VSensitiveDetector(name)
{
  //  G4cout << "*****************Sensitive Detector CONSTRUCTOR..................................... "<<G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G04SensitiveDetector::~G04SensitiveDetector()
{
  //  G4cout << "Sensitive Detector DESTRUCTOR..................................... "<<G4endl;

  //auto analysisManager = G4AnalysisManager::Instance();
////analysisManager->Write();
////analysisManager->CloseFile();
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void G04SensitiveDetector::Initialize(G4HCofThisEvent*)
{

  // EVT_WATCH = 0;  //// WATCH THIS EVENT   in  global_info

  G4int evt = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  //  G4cout << "Sensitive Detector Initialize.....................................evt: "<<evt<<G4endl;
  Etot = 0.0;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool G04SensitiveDetector::ProcessHits(G4Step* step, G4TouchableHistory* h)
{

  //G4cout << "Sensitive Detector    HIT..................................... "<<G4endl;
  //  G4double de = s->GetDeltaEnergy();
  G4double edep = step->GetTotalEnergyDeposit();
  G4double dt = step->GetDeltaTime();
  dt=dt+step->GetPreStepPoint()->GetGlobalTime();   // for activation HUGE

  // G4double dt = step->GetPostStepPoint()->GetGlobalTime(); // previous is more correct

  G4String ss=" - ";
  //  G4String part="--";
  char part[10];  // Here I have the text to print

  const int maxelem = 34;
  const char* elem[maxelem]
  = { "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "33", "34" };


  auto touchable = step->GetPreStepPoint()->GetTouchable();
  auto motherPhysical = touchable->GetVolume(1); // 2022 5 11  mother
  auto physical = touchable->GetVolume();
  auto copyNo = physical->GetCopyNo();
  auto motherCopyNo = touchable->GetVolume(1)->GetCopyNo();

  if (h != 0){
    ss = h->GetVolume()->GetName();  // never gives somt
  }

  G4bool las = step->IsLastStepInVolume(); // escaped the volume
  G4bool fir = step->IsFirstStepInVolume();

  // # event number
  G4int evt = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();



  G4Track *track = step->GetTrack();
  //  track->SetTrackStatus(fStopAndKill); // I REMOVE ALL SECONDARY TRACKS ????
  G4bool abs = track->GetTrackStatus();
  sprintf(part,"%s","--");

  G4ParticleDefinition* particle = track->GetDefinition();
  G4double Et = track->GetTotalEnergy();  // plus mass
  G4double Ek = track->GetKineticEnergy();  // seems this is Ek after LOSSES

  //  if (evt>=4148487){
  //    G4cout<<evt<<"_______________________________"<< particle->GetAtomicMass() << G4endl;
  // }

  //  if ( (particle->GetAtomicMass()>0)   and (particle->GetAtomicNumber()+1<maxelem) ) {
  //    sprintf(part, "%d-%s", int(particle->GetAtomicMass() ), elem[int(particle->GetAtomicNumber() ) -1]  );
  //    //part="IO";
  //  }

  // if (particle == G4Electron::Definition()) { sprintf(part,"%s","e-");}
  // else if (particle == G4Positron::Definition()) { sprintf(part,"%s","e+");}
  // else if (particle == G4Gamma::Definition()) {  sprintf(part,"%s","ga");}
  // else if (particle == G4Proton::Definition()) {  sprintf(part,"%s","p0");}
  // else if (particle == G4Neutron::Definition()) {  sprintf(part,"%s","n0");}
  // else if ( (particle->GetAtomicMass()>0)   and (particle->GetAtomicNumber()+1<maxelem) ) {
  //   sprintf(part, "%d-%s", int(particle->GetAtomicMass() ), elem[int(particle->GetAtomicNumber() ) -1]  );
  //   //part="IO";
  // }else{
  //   sprintf(part,"%s", particle->GetParticleName() );
  // }

  //-------------------------------------------------------- PARTICLE DESCRIPBED ------------------------------
  //    if ((detailed_display) and (evt==200)){
  //      G4cout<<"... switching off detailed display..."<<G4endl;
  //      detailed_display = 0;
  //    }


  if ((detailed_display) || (evt==0) || (evt==EVT_WATCH) ){
    G4cout<<std::setw(8) << evt<<" __ "<<std::setw(2)<<track->GetParentID()<<":"<<std::setw(2)<<track->GetTrackID()<<" __ "<<std::setw(12) << particle->GetParticleName();
    //    <<std::setw(6)<<round(10000*Et)/10000
    G4cout<<": Eki="<<" "<<std::setw(7)<<round(10000*(Ek+edep))/10000<<"  DE=" << std::setw(6)<<round(10000*edep)/10000<< " " << "  f-l "<<fir <<"-"<< las<<"-"<<abs<<" ";
    G4cout<<"// "<<motherPhysical->GetName()<<":"<<motherCopyNo<<" "<<physical->GetName()<<":"<<copyNo;
    if (dt/s>1){
      G4cout<<"        t="<<round(10000*dt/s)/10000<<" s"<<G4endl;
    }else{
      G4cout<<"        t="<<round(10000*dt/ns)/10000<<" ns"<<G4endl;
    }
    //if (las){ G4cout<<G4endl;}
  }

  if (edep==0.) return true;
  Etot=1.0*Etot + 1.0*edep;
//  G4cout<< " ..."<< vol->GetName() <<G4endl;

//  if (evt>=76355){
//    G4cout<<evt<<"______________________________="<<G4endl;
//  }

  return true;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void G04SensitiveDetector::EndOfEvent(G4HCofThisEvent* hc)
{
  G4int prec = G4cout.precision(3);
  G4int evt = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();

  //  if (evt>=76355){
  //    G4cout<<evt<<"________= = = _________________"<<G4endl;
  //  }
  G4int kilo = 1000;
  if ( (evt<15) or ( evt == EVT_WATCH  ) ){
    G4cout << "End Of Event ........................ "<<std::setw(8)<< evt<<"  Etot="<<std::setw(7)<<Etot<<G4endl;

    //G4cout << "End Of Event ............................ "<<std::setw(6)<< evt<<"  Etot="<<Etot<<G4endl;
  }
  if ( evt%(100*kilo) == 0 ){
    G4cout << "End Of Event ........................ "<<std::setw(8)<< round(evt/1000)<<"k  Etot="<<std::setw(7)<<Etot<<G4endl;
    //G4cout << "End Of Event ............................ "<<std::setw(6)<< evt<<"  Etot="<<Etot<<G4endl;
  }

  // ============ JUST GET INSTANVE
  if (not detailed_display){

    auto analysisManager = G4AnalysisManager::Instance();
    analysisManager->FillNtupleIColumn(0, evt);
    analysisManager->FillNtupleDColumn(1, Etot);
    // //analysisManager -> FillNtupleIColumn(1, parentID);
    // //analysisManager -> FillNtupleIColumn(2, volumeID);
    // //analysisManager -> FillNtupleSColumn(4, particleName);
    analysisManager->AddNtupleRow();
    // // not here analysisManager->Write();
  }
  //  G4cout << "End Of Event ............................ x"<<G4endl;

}
