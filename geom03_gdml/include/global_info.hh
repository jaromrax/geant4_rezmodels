//#ifndef global_info_h
//#define global_info_h

extern G4String  inputFilename ;
extern G4String outputFilename ;
extern G4String  macroFilename ;
extern G4bool detailed_display;
extern G4int EVT_WATCH; // watch this event
//#endif
