// --------------------------------------------------------------
//      GEANT 4 - gdml_det
//
// --------------------------------------------------------------

#include <vector>


#include "G04PrimaryGeneratorAction.hh"
#include "G04ActionInitialization.hh"
#include "G04DetectorConstruction.hh"
#include "G04SensitiveDetector.hh"

#include "G4RunManagerFactory.hh"

#include "FTFP_BERT.hh"


#include "G4UImanager.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

#include "G4GDMLParser.hh"

// needed to add later for comp with G01
#include "G4LogicalVolumeStore.hh"
#include "G4MTRunManager.hh"

// my global definitions,,  detailed_display
#include "global_info.hh"
// decay physics
#include "PhysicsList.hh"
#include "G4SteppingVerbose.hh"


// ====================================== HADR03 =================
#include "G4ParticleHPManager.hh"
// ========================================== definitions for global_info ============
 G4String  inputFilename  = "input.gdml";
 G4String outputFilename  = "output.csv";
 G4String  macroFilename  = "vis.mac";
 G4bool detailed_display = true;  //  for vis.mac ... only

G4int EVT_WATCH = 7; //

// tuples == sensitive SD volumes
G4int sd_volumes=0;
std::map<G4String, G4int> tuple_map;
G4bool REACTION = false;  // set if reaction is detected

// ========================================== definitions for global_info ============





void print_aux(const G4GDMLAuxListType* auxInfoList, G4String prepend="      | ", G4String append=" |")
{
  for(std::vector<G4GDMLAuxStructType>::const_iterator
      iaux = auxInfoList->begin(); iaux != auxInfoList->end(); iaux++ )
    {
      G4String str=iaux->type;
      G4String val=iaux->value;
      G4String unit=iaux->unit;

      G4cout << prepend << str << " : " << val  << " " << unit << append << G4endl;

      if (iaux->auxList) print_aux(iaux->auxList, prepend + "|");
    }
  return;
}

// --------------------------------------------------------------

int main(int argc,char **argv)
{
G4cout <<" HELP: ...                                        ..."<< G4endl;
G4cout <<" HELP: ... export G4FORCENUMBEROFTHREADS=1        ..."<< G4endl;
G4cout <<" HELP: ... do /vis/disable     to go full speed   ..."<< G4endl;
G4cout <<" HELP: ...                                        ..."<< G4endl;
G4cout <<" HELP: ...                                        ..."<< G4endl;
G4cout <<" HELP: ...  I read   input.gdml     file and show the geometry... "<< G4endl;
G4cout <<" HELP: ...  Auxiliary ... SensDet Tracker  ... allows tracking .. "<< G4endl;
G4cout <<"................................................................. "<< G4endl;
G4cout <<"..   when no arguments, it will load input.gdml           ....... "<< G4endl;
G4cout <<"..   when one argument, it is geometry and vis.mac        ....... "<< G4endl;
G4cout <<"..   when two arguments... geometry and MACRO             ....... "<< G4endl;
G4cout <<".....when 3 ... output_name_csv   else output             ..... "<< G4endl;
G4cout <<"................................................................. "<< G4endl;


// inputFilename = "input.gdml";
// outputFilename = "output.csv";
// macroFilename = "vis.mac";

 if (argc>1){
   inputFilename = argv[1];
 }

 if (argc>2){
   macroFilename  = argv[2];
   G4String vismac = "vis.mac";
   if (G4StrUtil::icompare(vismac, macroFilename)==0 ){
     G4cout<<"________________  vis mac _______  detailed display true "<<G4endl;
   }else{
     detailed_display = false;
   }
 }

 if (argc>3){
   outputFilename = argv[3];
 }


  //choose the Random engine
  G4Random::setTheEngine(new CLHEP::RanecuEngine);
  CLHEP::HepRandom::setTheSeed(1);

  //use G4SteppingVerboseWithUnits
  G4int precision = 4;
  G4SteppingVerbose::UseBestUnit(precision);



  G4GDMLParser parser;

// Uncomment the following if wish to avoid names stripping
// parser.SetStripFlag(false);

// Uncomment the following and set a string with proper absolute path and
// schema filename if wishing to use alternative schema for parsing validation
// parser.SetImportSchema("");
  parser.SetOverlapCheck(true);
  G4cout<<" ooooooooo---------- parser read ------------ooooooooooooo<<<-- "<<inputFilename<<G4endl;
  parser.Read(inputFilename);

   // Get the pointer to the User Interface manager
   G4UImanager* UImanager = G4UImanager::GetUIpointer();

   //G4MTRunManager *runManager= new G4MTRunManager(); //MULTITHREADE MODE ... operates as above
   auto* runManager = G4RunManagerFactory::CreateRunManager(G4RunManagerType::Default);
   runManager->SetNumberOfThreads(1); // YOU OVERRIDE WITH  export G4FORCENUMBEROFTHREADS=x

   runManager->SetUserInitialization(new G04DetectorConstruction(parser ));
   //runManager->SetUserInitialization(new FTFP_BERT); // from gdml example
   // I KNOW I NEED TO RUN MY PHYSICS
   runManager->SetUserInitialization(new PhysicsList); // from decay example

   // User action initialization
   runManager->SetUserInitialization(new G04ActionInitialization());




  // Replaced HP environmental variables with C++ calls
  G4ParticleHPManager::GetInstance()->SetSkipMissingIsotopes( false );
  G4ParticleHPManager::GetInstance()->SetDoNotAdjustFinalState( true );
  G4ParticleHPManager::GetInstance()->SetUseOnlyPhotoEvaporation( true );
  G4ParticleHPManager::GetInstance()->SetNeglectDoppler( false );
  G4ParticleHPManager::GetInstance()->SetProduceFissionFragments( true );
  G4ParticleHPManager::GetInstance()->SetUseWendtFissionModel( false );
  G4ParticleHPManager::GetInstance()->SetUseNRESP71Model( false );


   //  UImanager->ApplyCommand(G4String("/process/verbose       0"));
   UImanager->ApplyCommand(G4String("/process/em/verbose    0"));
   UImanager->ApplyCommand(G4String("/process/had/verbose   0"));
   UImanager->ApplyCommand(G4String("/process/eLoss/verbose 0"));
   //  UImanager->ApplyCommand(G4String("/control/verbose  0"));
   UImanager->ApplyCommand(G4String("/run/verbose      0"));
   UImanager->ApplyCommand(G4String("/event/verbose    0"));
   UImanager->ApplyCommand(G4String("/hits/verbose     0"));
   UImanager->ApplyCommand(G4String("/tracking/verbose 0"));
   UImanager->ApplyCommand(G4String("/stepping/verbose 0"));

   runManager->Initialize();

   // Initialize visualization
   G4VisManager* visManager = new G4VisExecutive;
   visManager->Initialize();

   ///////////////////////////////////////////////////////////////////////
   //
   // Example how to retrieve Auxiliary Information
   //

   G4cout << std::endl;

   const G4LogicalVolumeStore* lvs = G4LogicalVolumeStore::GetInstance();
   std::vector<G4LogicalVolume*>::const_iterator lvciter;
   G4cout << std::endl;
   G4cout << "___Local  auxiliary info:____________________________" << std::endl;
   G4cout << std::endl;
   for( lvciter = lvs->begin(); lvciter != lvs->end(); lvciter++ )
   {
     G4GDMLAuxListType auxInfo = parser.GetVolumeAuxiliaryInformation(*lvciter);
  // now the '' auxiliary info
     if (auxInfo.size()>0){
       G4cout << "___Auxiliary Information is found for Logical Volume :  "
              << (*lvciter)->GetName() << G4endl;

       G4String pname=(*lvciter)->GetName();
       pname.replace( pname.size()-2,2,"_p");
       // volume name and tuple number;; 0==event;  1..x energies
       sd_volumes++;
       tuple_map[pname] = sd_volumes;
     }

     print_aux(&auxInfo);
   }

   // now the 'global' auxiliary info
   G4cout << "                       volumes: "<<sd_volumes<<std::endl;
   G4cout << "___Global auxiliary info:____________________________" << std::endl;
   G4cout << std::endl;

   print_aux(parser.GetAuxList()); //gives nothing??

   G4cout << std::endl;
   G4cout << "Global auxiliary info:_____________________END____" << std::endl;
   G4cout << std::endl;
   //
   // End of Auxiliary Information block
   //
   ////////////////////////////////////////////////////////////////////////


   //   runManager->BeamOn(0);

   // Detect interactive mode (if only one argument) and define UI session
   //
   // G4UIExecutive* ui = 0;
   // if ( argc == 2 ) {
   //   ui = new G4UIExecutive(argc, argv);
   // }
   // Process macro or start UI session
   if ( not detailed_display )   // batch mode or interactive mode
   {
     G4cout<<"======= ONLY EXECUTE       MACRO   --------------------------============="<<macroFilename<<G4endl;
     G4String command = "/control/execute ";
     // G4String fileName = argv[2];
     G4cout<<" ooooooooo----------- macro name  -----SHOOT US-------<<<"<<macroFilename<<G4endl;
     UImanager->ApplyCommand(command+macroFilename);
   }
   else           // interactive mode, vis.mac by default.....
   {
     G4cout<<"=================================================== UI Session===vis==="<<macroFilename<<G4endl;
     G4cout<<"=================================================== UI Session===vis==="<<macroFilename<<G4endl;
     G4UIExecutive* ui = new G4UIExecutive(argc, argv);
     G4cout<<" ooooooooo----------- macro name  ------------<<<"<<macroFilename<<G4endl;
     UImanager->ApplyCommand("/control/execute "+macroFilename);
     if (ui->IsGUI()) {
      UImanager->ApplyCommand("/control/execute gui.mac");
     }
     ui->SessionStart();
     delete ui;
   }

   G4cout<<"                                                                    ======END OF UI"<<G4endl;


   delete visManager;
   delete runManager;
   return 0;
}
