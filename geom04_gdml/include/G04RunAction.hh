
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "G4AnalysisManager.hh"
#include "globals.hh"


#include "global_info.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class DetectorConstruction;
class Run;
class PrimaryGeneratorAction;
//class HistoManager;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class RunAction : public G4UserRunAction
{
  public:
  //   RunAction(DetectorConstruction*, PrimaryGeneratorAction*);
  //  RunAction(DetectorConstruction*);
  RunAction();
   ~RunAction() override;

  public:
    G4Run* GenerateRun() override;
    void BeginOfRunAction(const G4Run*) override;
    void   EndOfRunAction(const G4Run*) override;

  private:
  //DetectorConstruction*      fDetector     = nullptr;
  //PrimaryGeneratorAction*    fPrimary      = nullptr;
  // I try G4
  G4Run*                       fRun          = nullptr;
  //    HistoManager*              fHistoManager = nullptr;
  G4AnalysisManager* fanalysisManager = nullptr;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
