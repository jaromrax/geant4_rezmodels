//#ifndef global_info_h
//#define global_info_h

#include <map>
#include <string>

extern G4String  inputFilename ;
extern G4String outputFilename ;
extern G4String  macroFilename ;
extern G4bool detailed_display;
extern G4int EVT_WATCH; // watch this event

extern G4int sd_volumes;
extern std::map<G4String, G4int> tuple_map;

extern G4bool REACTION;
//#endif
