geant4~rezmodels~ {#geant4_rezmodels}
=================

`Warning: look somewhere else for a high quality simulation code for Geant written by full time expert`

Geant is quite complex beast with respect to some other - more friendly
- simulation tools. This repo\'s `purpose is to try to keep a track` of
attempts to get simplified+functional code for MeV energy nuclear
physics.

The focus here is (may change in future) on:

-   user friendly geometry input (PY generated gdml files at the moment)
-   decay (rdecay01 example)
-   ion beam (hadr04 example)
-   scalable data output across PC\'s (csv seems easy)
-   visualizing particular event from a large set

Getting started
---------------

Get to the desired geometry folder and call
`../00_pyg4om_code/geant-geo.py`.

-   building new geometry - `input.gdml`,
-   compilation,
-   run,
-   csv output merge (when `run` parameter to call `run.macro` is given)

are all done.

License
-------

The licence for this project just follows the Geant4 licence guidelines.
All added code (above the original examples) is a voluntary contribution
and is subjected to the same licence.

**Geant licence excerpt:**

Copyright (c) Copyright Holders of the Geant4 Collaboration, 1994-2006.
See <http://cern.ch/geant4/license> for details on the copyright
holders. All rights not expressly granted under this license are
reserved.

Project status
--------------

All development
