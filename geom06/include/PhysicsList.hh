
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef PhysicsList_h
#define PhysicsList_h 1

// #include "G4VUserPhysicsList.hh"

#include "G4VModularPhysicsList.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// //class PhysicsList: public G4VUserPhysicsList
// {
//   public:
//     PhysicsList();
//    ~PhysicsList() override = default;

//   protected:
//     // Construct particle and physics
//     void ConstructParticle() override;
//     void ConstructProcess()  override;
// };

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class PhysicsList: public G4VModularPhysicsList
{
public:
  PhysicsList();
 ~PhysicsList() override = default;

public:
  void ConstructParticle() override;
  void SetCuts() override;
  //  void ConstructProcess()  override;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
