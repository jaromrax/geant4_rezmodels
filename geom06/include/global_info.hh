//#ifndef global_info_h
//#define global_info_h

#include <map>
#include <string>

extern G4String  inputFilename ;
extern G4String outputFilename ;
extern G4String  macroFilename ;


extern G4int MAX_EVENTS_DISP; // watch this event
extern G4int WATCH_EVT; // watch this event
extern G4bool WATCH_REACTIONS; // print reactions anyrunvis
extern G4String WATCH_PARTICLE; // lookfor triton and report to tuple

// ----- this is number of sensitive volumes (found in main) and parameter name
extern G4int sd_volume_number;
extern std::map<G4String, G4int> tuple_map;

extern G4bool is_reaction;
extern G4bool detailed_display;


//#endif
