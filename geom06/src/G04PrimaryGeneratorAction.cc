//

#include "G04PrimaryGeneratorAction.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"



#include "G4IonTable.hh"
#include "G4Gamma.hh"
#include "G4Geantino.hh"

// searching CLHEP HepRandom
//#include "G4SystemOfUnits.hh"
//#include "G4AnalysisManager.hh"
//#include "G4ios.hh"

#include "G4RunManager.hh"
#include "global_info.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G04PrimaryGeneratorAction::G04PrimaryGeneratorAction()
 : G4VUserPrimaryGeneratorAction(),
   fParticleGun(0)
{
  G4int n_particle = 1;
  fParticleGun = new G4ParticleGun(n_particle);

  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;

  //G4ParticleDefinition* gea = particleTable->FindParticle(particleName="geantino");
  //G4ParticleDefinition* gea = G4Gamma::Definition();
  G4ParticleDefinition* gea = G4Geantino::Definition();
  G4ParticleDefinition* ion = G4IonTable::GetIonTable()->GetIon(133, 56, 0.);

  // only geantino works....
  fParticleGun->SetParticleDefinition( gea );


  fParticleGun->SetParticleEnergy(0.1*MeV);
  //doesnt work
  // later //  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(1.,0.,0.));
  fParticleGun->SetParticlePosition(G4ThreeVector(0*m, 0.0, 0.0) );
  // all is override in vis.mac.... but direction doesnt work
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G04PrimaryGeneratorAction::~G04PrimaryGeneratorAction()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//
// ----------------------- this overrides macro
//
void G04PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  /*
CONSISTENT -----------
1 53976885 20666
1 31672604 49321159
1 4559664 65678564
...
1 14254204 9034681
...
1 71828074 48839749
  */

  //  G4long fEventSeeds [2] = {68767034, 38925365};
  //  CLHEP::HepRandom::setTheSeeds( fEventSeeds );

   G4long fEventSeedIndex = CLHEP::HepRandom::getTheSeed();
   G4long fEventSeed1 = CLHEP::HepRandom::getTheSeeds()[0];
   G4long fEventSeed2 = CLHEP::HepRandom::getTheSeeds()[1];

    G4ParticleDefinition* particle = fParticleGun->GetParticleDefinition();
   if ( (detailed_display)and(is_reaction) ){
     G4cout<<"         ... Ranecu Seeds:"<<fEventSeedIndex<<" "<<fEventSeed1<<" "<<fEventSeed2<<G4endl;
     //  G4cout<<" --------------PGA  generate------------------"<<G4endl;
     //CLHEP::HepRandom::showEngineStatus();
     //G4cout<<"-->"<<particle->GetParticleName()<<G4endl;
    }
    // G4int i = anEvent->GetEventID() % 3;
  // G4ThreeVector v(1.0,0.0,0.0);
  // switch(i)
  // {
  //   case 0:
  //     break;
  //   case 1:
  //     v.setY(0.1);
  //     break;
  //   case 2:
  //     v.setZ(0.1);
  //     break;
  // }
  // fParticleGun->SetParticleMomentumDirection(v);
  fParticleGun->GeneratePrimaryVertex(anEvent);
  //  G4cout<<" --------------PGA  generate------------------"<<G4endl;

}
