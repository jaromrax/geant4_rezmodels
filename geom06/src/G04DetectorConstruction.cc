//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file G04DetectorConstruction.cc
/// \brief Implementation of the G04DetectorConstruction class

#include "G04DetectorConstruction.hh"
#include "G04SensitiveDetector.hh"
#include "G4SDManager.hh"
#include "G4GDMLParser.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G04DetectorConstruction::G04DetectorConstruction(const G4GDMLParser& parser)
 : G4VUserDetectorConstruction(),
   fParser(parser)
{
  G4cout<<"=================detector construction called============================="<<G4endl;
  fWorld = parser.GetWorldVolume();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* G04DetectorConstruction::Construct()
{
  //  G4cout<<"================ G04DetectorConstruction::Construct()  ==========================="<<G4endl;
  return fParser.GetWorldVolume();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void G04DetectorConstruction::ConstructSDandField()
{
  //------------------------------------------------
  // Sensitive detectors
  //------------------------------------------------
    G4cout<<"================ G04DetectorConstruction::ConstructSDandField()  ==========================="<<G4endl;

  G4SDManager* SDman = G4SDManager::GetSDMpointer();

  G4String trackerChamberSDname = "Tracker";
  G04SensitiveDetector* aTrackerSD =
    new G04SensitiveDetector(trackerChamberSDname);
  SDman->AddNewDetector( aTrackerSD );

  ///////////////////////////////////////////////////////////////////////
  //
  // Example how to retrieve Auxiliary Information for sensitive detector
  //
  G4cout<<G4endl<<"******************************************************************************** SENS DET"<<G4endl;
  const G4GDMLAuxMapType* auxmap = fParser.GetAuxMap();
  G4cout << "i... Found " << auxmap->size()
            << " volume(s) with auxiliary information:"
	 << G4endl; // << G4endl;
  for(G4GDMLAuxMapType::const_iterator iter=auxmap->begin();
      iter!=auxmap->end(); iter++)
  {
    G4cout << " ...                Volume " << ((*iter).first)->GetName()
           << " has the following list of auxiliary information: ";
    for (G4GDMLAuxListType::const_iterator vit=(*iter).second.begin();
         vit!=(*iter).second.end(); vit++)
    {
      G4cout << "--> Type: " << (*vit).type
                << " Value: " << (*vit).value << G4endl;
    }
  }
  G4cout << G4endl;
  G4cout<<"******************************************************************************** SENS DET"<<G4endl;

  // The same as above, but now we are looking for
  // sensitive detectors setting them for the volumes
  G4cout<<"____________ now looking for sensitive information _____"<<G4endl;
  for(G4GDMLAuxMapType::const_iterator iter=auxmap->begin();
      iter!=auxmap->end(); iter++)
  {
    G4cout << "i... Volume " << ((*iter).first)->GetName()
           << " has the following list of auxiliary information: "
           << G4endl;
    for (G4GDMLAuxListType::const_iterator vit=(*iter).second.begin();
         vit!=(*iter).second.end();vit++)
    {
      if ((*vit).type=="SensDet")
      {
        G4cout << " ... ... ... Attaching sensitive detector value=" << (*vit).value
               << " to volume =" << ((*iter).first)->GetName()
               <<  G4endl ;

        G4VSensitiveDetector* mydet =
          SDman->FindSensitiveDetector((*vit).value);
        if(mydet)
        {
          G4LogicalVolume* myvol = (*iter).first;
          myvol->SetSensitiveDetector(mydet);
        }
        else
        {
          G4cout << (*vit).value << " ##!!##!! detector not found" << G4endl;
        }
      }
    }
  }
    G4cout<<"____________ Finished with sensitive detectors _________"<<G4endl<<G4endl;
  G4cout<<"******************************************************************************** SENS DET"<<G4endl<<G4endl;

}
