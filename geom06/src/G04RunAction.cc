
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G04RunAction.hh"
// #include "Run.hh"
// #include "DetectorConstruction.hh"
// #include "PrimaryGeneratorAction.hh"
// #include "HistoManager.hh"

//maybe this exists
 #include "G4Run.hh"
// #include "G4RunManager.hh"
// #include "G4UnitsTable.hh"
// #include "G4SystemOfUnits.hh"

// #include "Randomize.hh"
// #include <iomanip>
 #include "G4AnalysisManager.hh"


// outputFilename
#include "global_info.hh"
#include "G4SDManager.hh"  // dont know what is that
#include <map>
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//RunAction::RunAction(DetectorConstruction* det, PrimaryGeneratorAction* prim) : fDetector(det) , fPrimary(prim)
//RunAction::RunAction(DetectorConstruction* det) : fDetector(det) //, fPrimary(prim)
RunAction::RunAction()  //, fPrimary(prim)
{
 // Book predefined histograms
  // fHistoManager = new HistoManager();

   G4cout<<" ______________ contructor.. @ runaction     _____________________"<<G4endl;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction()
{
  // delete fHistoManager;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Run* RunAction::GenerateRun()   // already runs many times
{
  //  G4cout<<" ______________ generate run @ runaction     _____________________"<<G4endl;
  //   fRun = new Run(fDetector);
  //   return fRun;
  fRun = new G4Run();
  return fRun;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::BeginOfRunAction(const G4Run*)
{
  G4cout<<" ______________ beginOfRunAction @ runaction   and TUPLE  creation________"<<G4endl;
  // // save Rndm status
  // G4RunManager::GetRunManager()->SetRandomNumberStore(false);
  //if (isMaster) G4Random::showEngineStatus();

  // // keep run condition
  // if (fPrimary) {
  //   G4ParticleDefinition* particle
  //     = fPrimary->GetParticleGun()->GetParticleDefinition();
  //   G4double energy = fPrimary->GetParticleGun()->GetParticleEnergy();
  //   fRun->SetPrimary(particle, energy);
  // }

  // //histograms
  // //

  // //  auto
  fanalysisManager = G4AnalysisManager::Instance();


  fanalysisManager->SetFileName("jarous");
  fanalysisManager->SetVerboseLevel(1);
  fanalysisManager->SetNtupleMerging(true); // true creates on rootfile
  if (not detailed_display){
    //fanalysisManager = G4AnalysisManager::Instance();  //fanalysisManager->SetFileName("jarous");    //fanalysisManager->SetVerboseLevel(1);
    fanalysisManager->SetActivation(true);  // enable inactivation of histograms
    // IF ROOT ... OK
    // if (1==1){      fanalysisManager->SetNtupleMerging(true);    }
    // if file is root .... it merges    //G4cout<<" ______________ beginOfRunAction @ runaction     _OPENFILE HERE?___________"<<G4endl;
    fanalysisManager->OpenFile( outputFilename); //   cat Output_nt*.csv > merged.csv; visidata  --header=0 merged.csv     //    _nt_test_tXX    is ADDED  ??
    fanalysisManager->CreateNtuple("sim", "Id_and_edep");
    fanalysisManager->CreateNtupleIColumn("id");

    std::map<G4String, G4int>::iterator it = tuple_map.begin();
    G4cout<< "         tuple_map size: "<<tuple_map.size() << G4endl;
    // Iterate through the map and print the elements
    G4cout<<"************************************ CREATING NTUPLE ****************************"<<G4endl;
    G4cout<<"************************************ CREATING NTUPLE ****************************"<<G4endl;
    while (it != tuple_map.end()) {
        G4cout << "Double TUPLE CREATION:   KEY!!!: " << it->first
             << " ....  Value  (not important): " << it->second << G4endl;
	fanalysisManager->CreateNtupleDColumn(it->first);
        ++it;
    }
    G4cout<<"************************************ CREATED NTUPLE ****************************"<<G4endl;
    G4cout<<"************************************ CREATED NTUPLE ****************************"<<G4endl;

    //    # ------- I WANT TO HAVE CHAIN AND DIFFERENT CONDITIONS ----------------------------
    /*
  fanalysisManager->CreateNtupleIColumn("tgta");  //
  fanalysisManager->CreateNtupleIColumn("tgtz");  //
  fanalysisManager->CreateNtupleIColumn("watcha");  //  triton
  fanalysisManager->CreateNtupleIColumn("watchz");  //  triton
  fanalysisManager->CreateNtupleIColumn("reac");  //
  fanalysisManager->CreateNtupleIColumn("reacsub");  //
    */
    //========== THREE volumes  for tritons =============:
    //  ---- corresponds to things in g0$SensitiveDetector.cc ===========
    fanalysisManager->CreateNtupleDColumn("eini");  // initial energy

    fanalysisManager->CreateNtupleSColumn("primary");  // prim particle
    fanalysisManager->CreateNtupleSColumn("watched");  // reaction target
    fanalysisManager->CreateNtupleSColumn("target");  // reaction target

    fanalysisManager->CreateNtupleIColumn("firstv");  //#CreateNtuple Column(it->fir
    fanalysisManager->CreateNtupleIColumn("leavev");
    fanalysisManager->CreateNtupleIColumn("stopv");

    fanalysisManager->CreateNtupleIColumn("prmax");
    //fanalysisManager->CreateNtupleDColumn("edep");
    fanalysisManager->FinishNtuple();


  }else{
    G4cout<<" ......................  no tuples but yes detailed display "<<G4endl;
  }
  // //analysisManager -> CreateNtuple("step", "step");
  // //analysisManager -> CreateNtupleIColumn("parentID");
  // //analysisManager -> CreateNtupleIColumn("volumeID");
  // //analysisManager -> CreateNtupleSColumn("particle name");

  // //G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  // if ( fanalysisManager->IsActive() ) { // NOT GOING HERE!!!!!!!!
  //   //   analysisManager->OpenFile();
  // }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::EndOfRunAction(const G4Run*)
{

  // // ==================== not clear how this can be used; HC seens to exist ====
  // if (isMaster){
  //   G4cout<<" ... fRun NoE   :"<<fRun->GetNumberOfEvent()<<G4endl;
  //   G4cout<<" ... fRun NoE2bp:"<<fRun->GetNumberOfEventToBeProcessed()<<G4endl;
  //   G4cout<<" ... fRun  hc   :"<<fRun->GetHCtable()<<G4endl;
  //   G4cout<<" ... fRun  dc   :"<<fRun->GetDCtable()<<G4endl;
  //   G4SDManager* SDMan = G4SDManager::GetSDMpointer();
  //   //    for(int i=0; i < SDMan->GetHCtable()->entries(); i++)
  //   G4int nentr = fRun->GetHCtable()->entries();
  //   G4int nent2 = SDMan->GetHCtable()->entries();
  //   G4cout<< " ... HCtab entries :"<<nentr<<" "<<nent2<<G4endl;
  //   for(int i=0; i < fRun->GetHCtable()->entries(); i++)
  //     {
  //       G4cout << i << " "
  //              << SDMan->GetHCtable()->GetSDname(i) << " "
  //              << SDMan->GetHCtable()->GetHCname(i) << " "<<G4endl;
  // 	  }
  // }



  // //save histograms
  //  analysisManager->Write();
  //  analysisManager->CloseFile();
 // if ( fanalysisManager->IsActive() ) {
  //  G4cout<<"<<<<<<<<<<<<<<<< analysis manager active<<<<<"<<G4endl;
  // G4cout<<" ______________ endOfRunAction @ runaction     ___________"<<fRun->GetRunID()<<G4endl;
  if (not detailed_display){
    //    G4cout<<"<<<<<<<<<<<<<<<< analysis manager active<<<<< closefile "<<G4endl;
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    fanalysisManager->Write();
    fanalysisManager->CloseFile();
  }
  // }

  // // show Rndm status
  // if (isMaster) G4Random::showEngineStatus();
  // if (isMaster) fRun->EndOfRun();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
